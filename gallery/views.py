from django.shortcuts import render

from gallery.models import GalleryImage
from django.views.generic import TemplateView

# Create your views here.

class HomePageView(TemplateView):
    
    model = GalleryImage
    template_name = 'gallery/gallery_home.html'
    
    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        
        lListOfAllTags = {}
        lImages = GalleryImage.objects.all().order_by('-date_taken')
        for image in lImages:
            lTags = image.tags.split(' ')
            for tag in lTags:
                try:
                    lCount = lListOfAllTags[tag]
                except KeyError:
                    lCount = 0
                lCount += 1
                lListOfAllTags[tag] = lCount
        
        context['Images'] = GalleryImage.objects.all().order_by('?')[:6]
        context['Tags'] = lListOfAllTags
        return context
    
home = HomePageView.as_view() 