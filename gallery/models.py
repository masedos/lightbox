from __future__ import unicode_literals

from django.db import models
from datetime import datetime, date
from gallery.thumbs import ImageWithThumbsField

# Create your models here.

class GalleryImageQuerySet(models.query.QuerySet):
    def active(self):
        return self.filter(active=True)

class GalleryImageManager(models.Manager):
    def get_queryset(self):
        return GalleryImageQuerySet(self.model, using=self._db)
		
    def all(self, *args, **kwargs):
        return super(GalleryImageManager, self).all(*args, **kwargs).active()



class GalleryImage(models.Model):
    """
    A photo in the gallery
    
    """
    last_modified = models.DateTimeField(default=datetime.now,editable=False)
    created = models.DateTimeField(default=datetime.now,editable=False)
    date_taken = models.DateField(default=date.today)
    DATE_RESOLUTION_CHOICES = (
        ('T', 'Hoje'),
        ('M', 'Mês e Ano'),
        ('Y', 'Anos'),
    )
    date_resolution = models.CharField(max_length=1, default='T', choices=DATE_RESOLUTION_CHOICES)
    photo = ImageWithThumbsField(upload_to='gallery', sizes=((150,150),(800,600)))
    name = models.CharField(max_length=50, help_text="Image name")
    tags = models.CharField(max_length=200, help_text="Space separated words")
    active = models.BooleanField(default=True)
    
    objects = GalleryImageManager()

    def __str__(self):
        return "{}".format(self.name)

    @property
    def display_date(self):
        lFunctions = {
         'T': format(self.date_taken, "jS M Y"),
         'M': format(self.date_taken, "M Y"),
         'Y': format(self.date_taken, "Y"),
         }
        return lFunctions[self.date_resolution]

    def save(self):
        self.last_modified = datetime.now()
        super(GalleryImage, self).save()

    class Meta:
        verbose_name = 'Gallery Image'
        verbose_name_plural = 'Galeria de Imagens'
        ordering = ['-created']