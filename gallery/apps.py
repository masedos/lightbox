from django.apps import AppConfig


class GalleryImageConfig(AppConfig):
    name = 'gallery'
    verbose_name = 'Galeria de Imagens'

