from django.contrib import admin

from gallery.models import GalleryImage

# Register your models here.


class GalleryImageAdmin(admin.ModelAdmin):
    list_display = ['name', 'tags', 'active', 'last_modified', 'created', 'date_taken', 'date_resolution', 'photo',  ]
    ordering = ['last_modified']
    search_fields = ['name', 'tags']
   
admin.site.register(GalleryImage, GalleryImageAdmin)