# Django Gallery - lightbox
Loja virtual - Projeto utilizado por [Fernandes Macedo](masedos@gmail.com)

![image](https://cloud.githubusercontent.com/assets/5832193/17952257/3ee3156e-6a3f-11e6-8add-6eeccbf68e3c.png)

## Instalação

### Clone o projeto
```
git clone https://bitbucket.org/masedos/lightbox
cd lightbox
```

### virtualenv
```
virtualenv env -p python3
```
Linux
```
source env/bin/activate
```
Windows
```
env\Scripts\activate.bat
```



## Instale as dependencias
```
pip install -r requirements.txt
python manage.py runserver
```

#fonte: http://drumcoder.co.uk/blog/2011/aug/23/jquery-gallery-lightbox/